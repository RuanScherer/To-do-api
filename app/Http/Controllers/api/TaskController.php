<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Task;
use App\Project;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tasks = Task::all();
      if($tasks != null)
      {
        return response()->json([
            'tasks' => $tasks,
            'status' => 'success'
        ]);
      }
      return response()->json(['status' => 'fail']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  		$project = Project::find($request->project_id);
  		if($project != null) 
  		{
  			if(Task::create($request->all()))
        {
          return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'fail']);
  		}
      return response()->json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $task = Task::find($id);
      if($task != null)
      {
        return response()->json([
            'task' => $task,
            'status' => 'success'
        ]);
      }
      return response()->json(['status' => 'fail']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByProject($id)
    {
      $task = Task::where('project_id', $id)->orderBy('priority')->orderBy('created_at', 'desc')->get();
      if($task != null)
      {
        return response()->json([
            'task' => $task,
            'status' => 'success'
        ]);
      }
      return response()->json(['status' => 'fail']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        if($task != null)
        {
            if($task->update($request->all()))
            {
                return response()->json(['status' => 'success']);
            }
        }
        return response()->json(['status' => 'fail']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        if($task != null)
        {
            if($task->delete())
            {
                return response()->json(['status' => 'success']);
            }
        }
        return response()->json(['status' => 'fail']);
    }
}
