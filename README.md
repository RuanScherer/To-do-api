# To-do API

This API contains:

- Projects management
- Tasks management

Consumed by [to-do with Vue](https://github.com/RuanScherer/To-do)