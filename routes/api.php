<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\ProjectController;

Route::get('projects', 'api\ProjectController@index');
Route::get('projects/{id}', 'api\ProjectController@show');
Route::post('projects', 'api\ProjectController@store');
Route::put('projects/{id}', 'api\ProjectController@update');
Route::delete('projects/{id}', 'api\ProjectController@destroy');

Route::get('tasks', 'api\TaskController@index');
Route::get('tasks/{id}', 'api\TaskController@show');
Route::get('tasks/project/{id}', 'api\TaskController@showByProject');
Route::post('tasks', 'api\TaskController@store');
Route::put('tasks/{id}', 'api\TaskController@update');
Route::delete('tasks/{id}', 'api\TaskController@destroy');
